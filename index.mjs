import fetch, { Headers } from "node-fetch";
import "cli-progress";
import fs from "fs";
import cliProgress from "cli-progress";
import { Parser, transforms } from "json2csv";
import 'dotenv/config'
const {flatten} = transforms

console.info("Started");

// # Personal API Key
const apiKey = process.env.APIKEY
// # Project ID
const projectID = process.env.PROJECTID
// # Limit fetch pages (0 = off) (testing only)
const pageLimit = 0;
// # Limit fetch profiles per page (0 = off) (testing only)
const profileLimit = 0;

const getHeaders = new Promise((resolve) => {
	console.info("Building headers..");
	const headers = new Headers();
	headers.append("x-apikey", apiKey);
	headers.append("Content-Type", "application/json");
	resolve(headers);
});

const headers = await getHeaders;

const fetchStats = new Promise(async (resolve) => {
	console.info("Fetching stats..");
	const req = await fetch(
		`https://api.datatrics.com/2.0/project/${projectID}/profile`,
		{ headers }
	);
	const res = await req.json();

	resolve(res);
});

let stats = await fetchStats;

const fetchUpdatedStats = new Promise(async (resolve) => {
	console.info("Fetching updated stats..");
	const req = await fetch(
		`https://api.datatrics.com/2.0/project/${projectID}/profile?limit=${stats.total_elements}`,
		{ headers }
	);
	const res = await req.json();

	resolve(res);
});

stats = await fetchUpdatedStats;

const fetchItems = new Promise(async (resolve, reject) => {
	const fetchingBar = new cliProgress.SingleBar({
		format: `Fetching items | {bar} | {percentage}% || {value}/{total} Pages`,
		barCompleteChar: "\u2588",
		barIncompleteChar: "\u2591",
		hideCursor: true,
	});

	const resolvingBar = new cliProgress.SingleBar({
		format: `Parsing items | {bar} | {percentage}% || {value}/{total} Pages`,
		barCompleteChar: "\u2588",
		barIncompleteChar: "\u2591",
		hideCursor: true,
	});

	const pages = Array(stats.total_pages).fill();
	const items = new Set();

	fetchingBar.start(stats.total_pages, 0);
	const promises = pages.map((val, i) => {
		++i;
		fetchingBar.increment();
		if (pageLimit && i > pageLimit) return console.warn(`Skipping ${i}!`);

		return new Promise(async (resolve) => {
			const req = await fetch(
				`https://api.datatrics.com/2.0/project/${projectID}/profile?page=${i}${
					profileLimit && `&limit=${profileLimit}`
				}`,
				{ headers }
			);
			try {
				const res = await req.json();
				resolve(res.items);
			} catch (error) {
				resolve(undefined);
			}
		});
	});

	fetchingBar.stop();
	console.info("Fetching complete!");

	resolvingBar.start(stats.total_pages, 0);
	for await (let promise of promises) {
		if (!promise) continue;
		await new Promise((resolve) => setTimeout(() => resolve(), 500));
		const item = await promise;
		item ? items.add(item) : console.error("Error occured!");
		resolvingBar.increment();
	}

	resolvingBar.stop();
	console.info("Parsing complete!");

	resolve(items);
});

const items = await fetchItems;

const saveItems = new Promise((resolve) => {
	const object = [...items].flat(Infinity);
	const json2csvParser = new Parser({ transforms: [ flatten('__')] });
	const csv = json2csvParser.parse(object);
	
	console.info(`Writing ${items.size} items to file..`);
	fs.writeFile("items.csv", csv, resolve);
});

await saveItems;

console.info("Finished!");
